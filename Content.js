import React from 'react';
import PropTypes from 'prop-types';

import Editable from './Editable';

import './Content.scss';

/**
 * Content
 * @description [description]
 * @example
  <div id="Content"></div>
  <script>
	ReactDOM.render(React.createElement(Components.Content, {
		content			: `Example x`,
		className		: 'foo'
	}), document.getElementById("Content"));
  </script>
 */
const Content = props => {
	return <Editable {...props} baseClass="content" />;
};

Content.defaultProps = {
	...Editable.defaultProps,
	...{
		tag: 'div'
	}
};

Content.propTypes = {
	...Editable.propTypes,
	...{}
};

export default Content;
