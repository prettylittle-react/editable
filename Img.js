import React from 'react';
import PropTypes from 'prop-types';

import Settings from 'settings';
import './Img.scss';

/**
 * Img
 * @description [Description]
 * @example
  <div id="Img"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Img, {
    	title : 'Example Img'
    }), document.getElementById("Img"));
  </script>
 */
class Img extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'image';
	}

	render() {
		const {src, height, width, useAsBackground, className, alt} = this.props;

		if (!src) {
			return null;
		}

		const imageSrc = Settings.config.usePlaceholders
			? `https://dummyimage.com/${width || 150}x${height || 150}`
			: src;

		const atts = {
			className: this.baseClass + (className ? ` ${className}` : ''),
			src: imageSrc,
			alt,
			height,
			width
		};

		if (useAsBackground) {
			return <div className={atts.className} style={{backgroundImage: `url(${atts.src})`}} />;
		}

		return <img {...atts} data-pin-nopin="true" />;
	}
}

Img.defaultProps = {
	className: '',
	useAsBackground: false,
	src: null,
	alt: null,
	height: null,
	width: null
};

Img.propTypes = {
	className: PropTypes.string,
	alt: PropTypes.string,
	useAsBackground: PropTypes.bool,
	src: PropTypes.string,
	height: PropTypes.number,
	width: PropTypes.number
};

export default Img;
