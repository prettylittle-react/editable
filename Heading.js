import React from 'react';
import PropTypes from 'prop-types';

import './Heading.scss';
/**
 * Heading
 * @description [description]
 * @example
  <div id="Heading"></div>
  <script>
	ReactDOM.render(React.createElement(Components.Heading, {
	}), document.getElementById("Heading"));
  </script>
 */
const Heading = props => {
	const {h, content, className, href} = props;

	if (!content) return '';

	const Tag = `h${h}`;
	const atts = {
		className: 'heading'
	};

	if (className) {
		atts.className = `heading ${className}`;
	}

	if (href) {
		return (
			<Tag {...atts}>
				<a href={href} dangerouslySetInnerHTML={{__html: content}} />
			</Tag>
		);
	}

	return <Tag {...atts} dangerouslySetInnerHTML={{__html: content}} />;
};

Heading.defaultProps = {
	h: 1,
	href: '',
	content: '',
	className: ''
};

Heading.propTypes = {
	h: PropTypes.number.isRequired,
	href: PropTypes.string,
	content: PropTypes.string,
	className: PropTypes.string
};

export default Heading;
