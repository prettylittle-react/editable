import React from 'react';
import PropTypes from 'prop-types';

/**
 * Editable
 * @description [Description]
 * @example
  <div id="Editable"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Editable, {
    	title : 'Example Editable'
    }), document.getElementById("Editable"));
  </script>
 */
const Editable = props => {
	const {content, children} = props;

	if (content === '' && !children) {
		return null;
	}

	const atts = {};

	if (props.className) {
		atts.className = props.className;
	}

	if (props.baseClass) {
		atts.className = atts.className ? `${props.baseClass} ${atts.className}` : props.baseClass;
	}

	if (props.title) {
		atts.title = props.title;
	}

	const Tag = props.tag;

	if (!content) {
		return <Tag {...atts}>{children}</Tag>;
	}

	return <Tag {...atts} dangerouslySetInnerHTML={{__html: content}} />;
};

Editable.defaultProps = {
	baseClass: '',
	title: undefined,
	tag: 'span',
	content: '',
	className: '',
	children: null
};

Editable.propTypes = {
	baseClass: PropTypes.string,
	title: PropTypes.string,
	tag: PropTypes.string.isRequired,
	html: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	className: PropTypes.string,
	children: PropTypes.node
};

export default Editable;
